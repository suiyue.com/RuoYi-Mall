package com.cyl.h5.pojo.dto;

import lombok.Data;

/**
 * @Author: czc
 * @Description: TODO
 * @DateTime: 2023/6/16 14:58
 **/
@Data
public class LoginDTO {
    private String account;
    private String password;
}
