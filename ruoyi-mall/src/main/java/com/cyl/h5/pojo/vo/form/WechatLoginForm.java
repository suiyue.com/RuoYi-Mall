package com.cyl.h5.pojo.vo.form;

import lombok.Data;

@Data
public class WechatLoginForm {
    private String code;
    private String state;
}
