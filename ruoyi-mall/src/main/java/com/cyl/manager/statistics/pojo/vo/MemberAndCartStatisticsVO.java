package com.cyl.manager.statistics.pojo.vo;

import lombok.Data;

@Data
public class MemberAndCartStatisticsVO {
    private Integer memberCount;
    private Integer cartCount;

}
