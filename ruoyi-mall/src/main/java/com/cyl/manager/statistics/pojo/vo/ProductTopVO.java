package com.cyl.manager.statistics.pojo.vo;

import lombok.Data;

@Data
public class ProductTopVO {

    private String productName;
    private int totalSales;
    private String pic;
    private String spData;
}
